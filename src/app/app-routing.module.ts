import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UsersComponent } from './users/users.component';
import { UserEditComponent } from './user-update/user-update.component';
import {LoginComponent} from "./login/login.component";

const routes: Routes = [
  	{ path: '', redirectTo: '/login', pathMatch: 'full' },
  	{ path: 'login', component: LoginComponent },
  	{ path: 'users', component: UsersComponent },
  	{ path: 'update/:id', component: UserEditComponent }
];

@NgModule({
  	imports: [ RouterModule.forRoot(routes) ],
  	exports: [ RouterModule ]
})
export class AppRoutingModule {}
