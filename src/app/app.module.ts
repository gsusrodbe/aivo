import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatCardModule} from '@angular/material/card';
import { MatGridListModule} from '@angular/material/grid-list';
import { MatDialogModule} from '@angular/material/dialog';
import { ModalModule } from 'ngx-bootstrap/modal';
import { LoginComponent } from './login/login.component';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { UsersService } from './services/user.service';
import { UsersComponent } from './users/users.component';
import { UserEditComponent } from './user-update/user-update.component';
import { StorageService } from './services/storage.service';
import { StorageServiceModule} from 'angular-webstorage-service';

@NgModule({
    declarations: [
        AppComponent,
        UsersComponent,
        UserEditComponent,
        LoginComponent
    ],
    imports: [
        BrowserModule,
        MatCardModule,
        MatGridListModule,
        MatDialogModule,
        FormsModule,
        AppRoutingModule,
        HttpClientModule,
        StorageServiceModule
    ],
    providers: [
  	    UsersService,
        StorageService

    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
