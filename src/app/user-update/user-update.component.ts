import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Location } from '@angular/common';

import { User }         from '../user';
import { UsersService } from '../services/user.service';
import { StorageService } from '../services/storage.service';

@Component({
	selector: 'app-user-update',
	templateUrl: './user-update.component.html',
	styleUrls: [ './user-update.component.css' ]
})
export class UserEditComponent implements OnInit {
	@Input() user: User;
	public users;

	constructor(
		private storageService: StorageService,
		private route: ActivatedRoute,
		private userService: UsersService,
		private location: Location
	) {}

	ngOnInit(): void {
		this.getUser();
	}

	getUser(): void {
		const id = +this.route.snapshot.paramMap.get('id');
		this.userService.getUser(id)
			.subscribe(user => this.user = user.data);
	}

	goBack(): void {
		this.location.back();
	}

 	update(): void {
		this.userService.updateUser(this.user)
			.subscribe(user => {
				this.users = (this.storageService.getFromLocal('dataUsers'));
				this.users.forEach((usr, usrKey)=>{
					if (usr.id == this.user.id){
						this.users.splice(usrKey, 1, user);
						this.storageService.saveInLocal('dataUsers', this.users);
					}
				})
				this.goBack();
			})
	}
}
