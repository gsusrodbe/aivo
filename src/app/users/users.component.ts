import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/pairwise';

import { User } from '../user';
import { UsersService } from '../services/user.service';
import { StorageService } from '../services/storage.service';

let usersPartial = [];

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css']
})


export class UsersComponent implements OnInit {

    public page = 1;

    users: User[];
    name: string;

    constructor(
        private storageService: StorageService,
        private userService: UsersService,
        private route: ActivatedRoute,
        private router: Router
        ) { }

    ngOnInit() {
        if(this.storageService.getFromLocal('dataUsers')){
            this.users = this.storageService.getFromLocal('dataUsers');
        } 
        else {
            this.getUsers(this.page);
        }
    }

    getUsers(page): void {
        this.userService.getUsers(page)
        .subscribe(users => {
            if(users.total_pages >= users.page){
                let dataArray = users.data
                dataArray.forEach(element => {
                    usersPartial.push(element)
                })
            this.getUsers(page + 1)
        } 
        else {
            this.users = usersPartial;
            this.storageService.saveInLocal('dataUsers', this.users)
        }
    });
}
    redirect(user: User): void {      
        this.router.navigate(['./update', user]);       
    }
    
    delete(user: User): void {
        this.users = this.users.filter(usr => usr !== user);
        this.userService.deleteUser(user).subscribe();
    }
}