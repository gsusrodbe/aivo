import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { User } from './../user';

// import { MessageService } from './message.service';

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class UsersService {

	private baseUrl = 'https://reqres.in/api/';  // URL to web api

	constructor(
		private http: HttpClient
		// private messageService: MessageService) 
	){ }

	/** GET users from the server */
		getUsers (page): Observable<any> {
				return this.http.get<any>(this.baseUrl + 'users?page=' + page)
						.pipe(
						catchError(this.handleError('getUsers', []))
				 );
		}

		deleteUser (user: User | number): Observable<User> {
			const id = typeof user === 'number' ? user : user.id;
			const url = `${this.baseUrl}users/${id}`;

			return this.http.delete<User>(url, httpOptions).pipe(
				// tap(_ => this.log(`deleted hero id=${id}`)),
				catchError(this.handleError<User>('deleteUser'))
			);
		}

		getUser(id: number): Observable<any> {
			const url = `${this.baseUrl}users/${id}`;
			return this.http.get<any>(url).pipe(
				// tap(_ => this.log(`fetched hero id=${id}`)),
				catchError(this.handleError<any>(`getUser id=${id}`))
			);
		}

		updateUser (user: User): Observable<any> {
			const url = `${this.baseUrl}users/${user.id}`;
			return this.http.put(url, user, httpOptions).pipe(
				// tap(_ => this.log(`updated hero id=${hero.id}`)),
				catchError(this.handleError<any>('updateUser'))
			);
		}

		searchUsers(term: string): Observable<User[]> {
		  	if (!term.trim()) {
		        return of([]);
		  	}
		  	return this.http.get<User[]>(`api/users/?name=${term}`).pipe(
		    	// tap(_ => this.log(`found heroes matching "${term}"`)),
		    	catchError(this.handleError<User[]>('searchUsers', []))
		  	);
		}

	private handleError<T> (operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {

			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			// console.log(`${operation} failed: ${error.message}`);

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a HeroService message with the MessageService */
	// private log(message: string) {
	//   this.messageService.add('HeroService: ' + message);
	// }
}
