import { Inject } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SESSION_STORAGE, WebStorageService} from 'angular-webstorage-service';

@Injectable()
export class StorageService {

	public data:any=[]
	constructor(
		@Inject(SESSION_STORAGE) private storage: WebStorageService
	){ }

    saveInLocal(key, val): void {
	    this.storage.set(key, val);
	    this.data[key]= this.storage.get(key);
    }

    getFromLocal(key): any {
        this.data[key]= this.storage.get(key);
        return this.data[key]
   	}
}